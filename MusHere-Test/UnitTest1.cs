﻿using MusHere_Lib;

namespace MusHere_Test;

public class UnitTest1
{
    [Fact]
    public void Test1()
    {
        Music musica = new Music();
        musica.Name = "Poco Loco";

        Assert.Equal("Poco Loco", musica.Name);
    }
}
