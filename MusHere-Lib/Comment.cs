﻿using System;
namespace MusHere_Lib
{
	public class Comment
	{
		public User? Creator { get; set; }

		public string? Content { get; set; }

		public DateTime? DateCreated { get; set; }

		public List<User>? LikedBy { get; set; }

		private int _NumberLikes = -1;
		public int? NumberLikes
		{
			get
			{
				if(_NumberLikes == -1)
				{
					_NumberLikes = LikedBy != null ? LikedBy.Count() : 0;
				}
				return _NumberLikes;
			}
		}

		public Comment()
		{
		}
	}
}

