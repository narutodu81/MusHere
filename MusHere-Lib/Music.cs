﻿namespace MusHere_Lib;
public class Music
{
    public int ID { get; set; }

    public string? Name { get; set; }

    public string? Author { get; set; }

    public string? ImageLink { get; set; }

    public string? FullName
    {
        get
        {
            return string.Format("{0} - {1}", Name, Author);
        }
    }
}

