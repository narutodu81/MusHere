﻿using System;
namespace MusHere_Lib
{
	public class User
	{
		public int Id { get; set; }

		public string Username { get; set; }

		public string Description { get; set; }


		public User()
		{
			Id = -1;
			Username = "Unkonwn";
			Description = "None";
		}	
	}
}

