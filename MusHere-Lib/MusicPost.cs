﻿using System;
namespace MusHere_Lib
{
	public class MusicPost
	{
		public int? ID { get; set; }

		public User? Creator { get; set; }

		public Music? MusicChoose { get; set; }

		public List<Comment>? Comments { get; set; }

		public DateTime? DateCreated { get; set; }

		public MusicPost()
		{
		}
	}
}

