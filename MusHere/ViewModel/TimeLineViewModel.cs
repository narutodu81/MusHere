﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MusHere_Lib;

namespace MusHere.ViewModel
{
    public class TimeLineViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private List<MusicPost> _timeline;

        public List<MusicPost> timeline
        {
            get => _timeline;
            set
            {
                _timeline = value;
                OnPropertyChanged();
            }
        }

        public string number = "-1";

        public TimeLineViewModel()
        {
            Init();
        }

        public void Init()
        {
            User monUtilisateurTest = new User()
            {
                Id = 1,
                Username = "Kamul0x",
                Description = "The Gamer"
            };

            Music maMusiquetest = new Music()
            {
                ID = 1,
                Name = "Save Your Tears",
                Author = "The Weeknd",
                ImageLink = "default_music.jpeg"
            };

            MusicPost musicPostTest = new MusicPost()
            {
                ID = -1,
                Creator = monUtilisateurTest,
                MusicChoose = maMusiquetest,
                DateCreated = DateTime.Now
            };

            timeline = new List<MusicPost>();
            timeline.Add(musicPostTest);
            timeline.Add(musicPostTest);
            timeline.Add(musicPostTest);
            timeline.Add(musicPostTest);
            timeline.Add(musicPostTest);
        }

        public void OnPropertyChanged([CallerMemberName] string name = " ") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }
}

