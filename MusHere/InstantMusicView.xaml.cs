﻿using MusHere_Lib;

namespace MusHere;

public partial class InstantMusicView : ContentView
{
    public MusicPost Post;
    public User user;

	public InstantMusicView()
	{
		InitializeComponent();
		//InitUser();

		//BindingContext = Post;
	}

    public InstantMusicView(MusicPost post, User user)
    {
        Post = post;
        this.user = user;
    }

    public void InitUser()
	{
        User monUtilisateurTest = new User()
        {
            Id = 1,
            Username = "Kamul0x",
            Description = "The Gamer"
        };

        Music maMusiquetest = new Music()
        {
            ID = 1,
            Name = "Save Your Tears",
            Author = "The Weeknd",
            ImageLink = "default_music.jpeg"
        };

        MusicPost musicPostTest = new MusicPost()
        {
            Creator = monUtilisateurTest,
            MusicChoose = maMusiquetest
        };
        //post = musicPostTest;
    }
}
