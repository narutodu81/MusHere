﻿using Microsoft.Maui.Controls.Maps;
using Microsoft.Maui.Maps;
using Map = Microsoft.Maui.Controls.Maps.Map;

namespace MusHere;

public partial class MapPage : ContentPage
{
	public MapPage()
	{
		//InitializeComponent();
		Location location = new Location(45.7578137, 4.8320114);
		MapSpan mapSpan = new MapSpan(location, 0.1, 0.1);
		Map map = new Map(mapSpan);

		Pin pin = new Pin
		{
			Label = "Muze 11234",
			Address = "Save Your Tears - The Weeknd",
			Type = PinType.Place,
			Location = new Location(45.760547, 4.861118)
		};
		map.Pins.Add(pin);

		Content = map;
	}
}
